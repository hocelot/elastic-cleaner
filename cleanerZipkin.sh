#!/bin/bash

# This script checks if there's any data older than 30 days

# VAR definition
CLEAN_DATE=$(date "+%Y-%m-%d" -d "-30 days")
AUTH_DEV=elastic:8WQDaEXbvpOWtmN0VI0ORJvw
AUTH_PRO=elastic:EfUwpFQnIU6HaiMDu1m6UIdo

# Checking if there's any index older than 30 days in DEV
/bin/echo "Checking Zipkin index..."
/usr/bin/curl --user $AUTH_DEV -X GET https://d4d10547a0f44d03c5b18c65ae4eff48.europe-west1.gcp.cloud.es.io:9243/zipkin:span-$CLEAN_DATE | /bin/grep '{"zipkin:span-$CLEAN_DATE"' &> /dev/null
if [ $? == 0 ]; then
    /bin/echo "The index exists, deleting... "
    /usr/bin/curl --user $AUTH_DEV -X GET https://d4d10547a0f44d03c5b18c65ae4eff48.europe-west1.gcp.cloud.es.io:9243/zipkin:span-$CLEAN_DATE | /bin/grep '{"acknowledged":true}' &> /dev/null
    if [ $? == 0 ]; then
        /bin/echo "Index removed."
    else
        /bin/echo "There was an error removing the index"
        exit 1
    fi
else
    /bin/echo "Nothing to do. Let's check PRO"
fi

# Checking if there's any index older than 30 days in PRO
/bin/echo "Checking Zipkin index..."
/usr/bin/curl --user $AUTH_PRO -X GET https://e26e312b079fec238fb0364c6efe6ee0.europe-west1.gcp.cloud.es.io:9243/zipkin:span-$CLEAN_DATE | /bin/grep '{"zipkin:span-$CLEAN_DATE"' &> /dev/null
if [ $? == 0 ]; then
    /bin/echo "The index exists, deleting... "
    /usr/bin/curl --user $AUTH_PRO -X GET https://e26e312b079fec238fb0364c6efe6ee0.europe-west1.gcp.cloud.es.io:9243/zipkin:span-$CLEAN_DATE | /bin/grep '{"acknowledged":true}' &> /dev/null
    if [ $? == 0 ]; then
        /bin/echo "Index removed."
        exit 0
    else
        /bin/echo "There was an error removing the index"
        exit 1
    fi
else
    /bin/echo "Nothing to do."
    exit 0
fi