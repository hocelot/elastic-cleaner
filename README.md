# README #

Elasticsearch cleaning scripts

### What is this repository for? ###

* Eliminate Elasticsearch's and Logstash's old data (we only want to store data from the last 30 days
* Version: 1.0

### How it works ###

These scripts are going to be launched from the included Jenkinsfile.

These scripts have the mission of keeping only the last 30 days of data in Logstash and Zipkin. To achieve this, the script launch a petition to Elasticsearch's API with a GET, to check the existance of the index and, if it exist, a DELETE statement.
